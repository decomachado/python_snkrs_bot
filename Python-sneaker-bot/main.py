import requests
from bs4 import BeautifulSoup
import csv
from dhooks import Webhook

webhook = Webhook('https://discord.com/api/webhooks/875536431777448017/pC78kF2v-D89iXwCufiW5ClPSN9rfdYGz97SQTIag9mwLsVsyxuTM3RH5VefKV_f5N90')

INVENTORY_FILE_NAME = 'inventory.csv'
FEED_FILE_NAME = 'feed.csv'

def get_inventory_request(page):

    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.76 Safari/537.36', "Upgrade-Insecure-Requests": "1","DNT": "1","Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8","Accept-Language": "en-US,en;q=0.5","Accept-Encoding": "gzip, deflate"}
    
    src_inventory = requests.get('https://www.nike.com.br/Snkrs/Estoque?p=' + str(page), headers=headers).text

    return BeautifulSoup(src_inventory, 'lxml')
    
    
def mount_inventory_data(product_inventory_list, page):
    
    response = get_inventory_request(page)
    
    buy_products = response.find_all('div', class_="produto produto--comprar")
    if len(buy_products) == 0: 
        return False
    
    for product in buy_products:
        product_name = product.div.div.h2.span.text
        product_href = product.div.a['href'] 
        list_of_validation = list(filter(lambda x: product_href in x, product_inventory_list))
        if not list_of_validation:
            product_inventory_list.append([product_name, product_href])
            notify_discord(product_href)

    return True

def get_inventory():
    
    page = 1
    inventory = get_stored_inventory()
    while True:
        
        response = mount_inventory_data(inventory, page)
        
        if not response:
            break
        page += 1

    return inventory

def write_csv(file_name, data):
    file = open(file_name, 'w')
    writer = csv.writer(file)
    
    for item in data:
        writer.writerow(item)

    file.close()

def get_stored_inventory():
    product_inventory_csv = open(INVENTORY_FILE_NAME, 'r+')
    product_inventory_reader = csv.reader(product_inventory_csv)
    product_inventory_list = []

    for line in product_inventory_reader:
        product_inventory_list.append(line)

    product_inventory_csv.truncate(0)
    product_inventory_csv.close()

    return product_inventory_list

def get_feed_request(page = 1):

    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.76 Safari/537.36', "Upgrade-Insecure-Requests": "1","DNT": "1","Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8","Accept-Language": "en-US,en;q=0.5","Accept-Encoding": "gzip, deflate"}

    src_feed = requests.get('https://www.nike.com.br/Snkrs/Feed?p=' + str(page), headers=headers).text
    return BeautifulSoup(src_feed, 'lxml')

def get_stored_feed():
    product_feed_csv = open(FEED_FILE_NAME, 'r+')
    product_feed_reader = csv.reader(product_feed_csv)
    product_feed_list = []

    for line in product_feed_reader:
        product_feed_list.append(line)
    product_feed_csv.truncate(0)
    product_feed_csv.close()

    return product_feed_list

def mount_feed_data(product_feed_list, page):
    response = get_feed_request(page)
    buy_products = response.find_all('div', class_="produto produto--aviseme")
    sold_out_products = response.find_all('div', class_="produto produto--esgotado")

    products = buy_products + sold_out_products
    if len(products) == 0: 
        return False
    
    for product in products:
        product_name = product.div.div.h2.span.text
        product_href = product.div.a['href'] 
        list_of_validation = list(filter(lambda x: product_href in x, product_feed_list))
        if not list_of_validation:
            product_feed_list.append([product_name, product_href])

    return True
    
def get_feed():
    page = 1
    feed = get_stored_feed()

    while True:
        response = mount_feed_data(feed, page)
        if not response:
            break
        page += 1

    return feed

def notify_discord(product_href):
    webhook.send(product_href)
    print("sent to discord")

def monitor():
    
    write_csv(INVENTORY_FILE_NAME, get_inventory())
    write_csv(FEED_FILE_NAME, get_feed())    

monitor()

